-- utiliities
local function union(...)
  local tables, result = {...}, {}
  for i = 1, #tables do
    for j = 1, #tables[i] do
      table.insert(result, tables[i][j])
    end
  end
  return result
end

-- Debugging global vars utilities
local _USE_INSPECT  = true
local _USE_DEBUG    = false
local _USE_PROFILER = false
local _USE_PRINT_TRACEBACK = false
local _PRINT_RUNNING_SYSTEMS = false

if _USE_PRINT_TRACEBACK then
  local original_print = print
  print = function(...)
    local traceback = debug.traceback()
    original_print(traceback .. '\n', ...)
  end
end

if _USE_INSPECT then
  insp = require 'inspect'
  print_insp = function (value, pre, pos)
    print((pre and pre .. " " or "")  .. insp(value) .. (pos and " " .. pos or "") )
  end
end

if _USE_DEBUG then
  require 'mobdebug'.start()
end

if _USE_PROFILER then
  profi = require 'ProFi'
  profi:start()
  PROFI_LIMIT = 100
  profi_ticks = 0
end

local function try_or_error(ok_value, err_msg)
  if not ok_value then
    error("try error: " .. tostring(err_msg))
  end
  return ok_value
end

local keys_pressed = {}

local entity = require "motor.entity"
local storage = require "motor.storage"
local system_data = require "motor.system_data"
local bitset_array = require "motor.bitset_array"

-- components
local components = require "tinta.components"
local systems = require "tinta.systems"

local _unpack = unpack or table.unpack

local function create_and_associate_component(state, ent, comp_name, ...)
  local new_comp = components.components[comp_name].new(...)
  local new_comp_gen_idx = state.storages[comp_name]:new_entry(new_comp)

  ent:associate(new_comp_gen_idx, state.masks[comp_name])

  return new_comp_gen_idx
end

local function create_entity(state, pos_x, pos_y, rot, parent_gen_idx)
  local new_entity = entity.new()
  local new_entity_gen_idx = try_or_error(state.storages.entities:new_entry(new_entity))

  if parent_gen_idx then
    local parent_entity = try_or_error(state.storages.entities:get_entry(parent_gen_idx))

    local parent_scene_graph_gen_idx =
      parent_entity.associated_components[
        parent_entity:get_storage_index(state.masks.scene_graph)
      ]

    local parent_scene_graph = try_or_error(
      state.storages.scene_graph:get_entry(parent_scene_graph_gen_idx)
    )

    table.insert(parent_scene_graph.children_gen_idxs, new_entity_gen_idx)
  end

  -- [[ create and associate scene_graph component
  create_and_associate_component(
    state,
    new_entity,
    'scene_graph',
    new_entity_gen_idx, parent_gen_idx -- can be nil, but is not a problem
  )
  --]]

  --[=[ create and associate anchored rect component
  local rect_anchored_id = create_and_associate_component(
    state,
    new_entity,
    'anchored_rect',
    {up = 0.80, left = 0.85, right = 0.90, bottom = 0.95},
    {up = 0010, left = 0020, right = 0030, bottom = 0040}
  )

  create_and_associate_component(
    state,
    new_entity,
    'final_anchored_rect',
    {up = 1, left = 1, right = 1, bottom = 1},
    {up = 1, left = 1, right = 1, bottom = 1}
  )

  -- [[
  create_and_associate_component(
    state,
    new_entity,
    'anchored_rect_viewer',
    {1, 1, 1, 1}, -- anchored rect color
    {0, 1, 0, 1}, -- canvas dimensions color
    {0, 0, 1, 1}, -- anchors color
    {1, 0, 1, 1}, -- offsets color
    true -- show numbers
  )
  --]]
  --]=]

  -- create and associate transform component
  create_and_associate_component(
    state,
    new_entity,
    'transform',
    pos_x, pos_y, rot, 1, 1
  )

  -- create and associate final transform component
  create_and_associate_component(
    state,
    new_entity,
    'final_transform',
    rect_anchored_id and true or false
  )

  -- [[ create and associate shape component
  local wh_scalar = rect_anchored_id and 1 or 32

  create_and_associate_component(
    state,
    new_entity,
    'shape',
    'rectangle',{width = wh_scalar, height = wh_scalar, offset = {x = 0, y = 0}}
    --'circle', {radius = 1, segments = 16, offset = {x = 0, y = 0}}
    --'polygon', {0,0, 0.5,0, 1,1, 0,1, offset = {x = 0, y = 0}}
  )
  --]]

  -- [[create and associate shape_to_mesh component
  create_and_associate_component(
    state,
    new_entity,
    'shape_to_mesh',
    {1, 1, 1, 0.3}
  )
  --]]

  -- [[ create and associate mesh component
  create_and_associate_component(
    state,
    new_entity,
    'mesh',
    4
  )
  --]]

  --[[ create and associate image component
  create_and_associate_component(
    state,
    new_entity,
    'image',
    love.image.newImageData('tinta/protoenemy.png')
  )
  --]]

  -- create and associate text component
  create_and_associate_component(
    state,
    new_entity,
    'text',
    "Olá Mundo :3 =^.^= ÒvÓ " .. new_entity_gen_idx.index
  )

  -- [[ create and associate intersection component
  create_and_associate_component(
    state,
    new_entity,
    'intersection'
  )
  --]]

  -- [[ pointer
  create_and_associate_component(
    state,
    new_entity,
    'pointer'
  )
  --]]

  -- [[ editable text
  --if parent_gen_idx then
    create_and_associate_component(
      state,
      new_entity,
      'editable'
    )
  --end
  --]]

  -- [=[ events
  create_and_associate_component(
    state,
    new_entity,
    'events',
    {
      {
        'text', {
          {'set_text_string', {'teste events'}}
        }
      }
    }
  )

  -- [[ timed events
  if not parent_gen_idx then
    create_and_associate_component(
      state,
      new_entity,
      'timed_events',
      {
        {
          time = 0.5,
          events = {
            {'text',
              {{'set_text_string', {'Hello :)'}}}}
          },
          used = false
        }, {
          time = 1,
          events = {
            {'text',
              {{'set_text_string', {'Hello :D'}}}}
          },
          used = false
        }, {
          time = 1.1,
          events = {
            {'timed_events', {{'start', {}}}}
          },
          used = false
        }
      },
      love.timer.getTime()
    )
  end
  --]]
  --]=]

  return new_entity_gen_idx
end

local test_state = {
  storages = {
    entities = storage.new(),
  },
  masks = {
    entities = bitset_array.new(1, {1}),
  },
  systems = {}
}

for i = 1, #components.names do
  local component_name = components.names[i]
  test_state.storages[component_name] = storage.new()
  test_state.masks[component_name] = bitset_array.new(1, {1}):lshift(i)
end

for i = 1, #systems.names do
  local system_name = systems.names[i]
  local system = systems.systems[system_name]

  local masks = {}
  for j = 1, #system.masks_names do
    local mask_name = system.masks_names[j]
    masks[j] = test_state.masks[mask_name]
  end

  test_state.systems[system_name] = system.new(_unpack(masks))
end

local entity_1 = create_entity(test_state, 20, 00, 0)
local entity_2 = create_entity(test_state, 10, 30, 0, entity_1)
local entity_3 = create_entity(test_state, 10, 40, 0, entity_2)

--print_insp(test_state.storages.entities)

local function run_system(state, system_name, ...)
  -- print ('running ' .. system_name)

  local system_data = state.systems[system_name]
  local system_lib = systems.systems[system_name]

  --print_insp(system_data)

  local storages = {}
  for i = 1, #system_lib.masks_names do
    storages[i] = state.storages[system_lib.masks_names[i]]
  end

  system_data:update(state.storages.entities)

  local storage_args = {_unpack(storages)}
  local extra_args = {...}
  local system_args = union(storage_args, extra_args)

  system_lib.run(system_data, _unpack(system_args))
end

local function run_systems(state, system_group, ...)
  if _PRINT_RUNNING_SYSTEMS then
    print('-- RUNNING ' .. system_group:upper():gsub('_', ' ') .. ' --')
  end

  for i = 1, #systems.names_by_step[system_group] do
    local system_name = systems.names_by_step[system_group][i]

    if _PRINT_RUNNING_SYSTEMS then
      print('\trunning "' .. system_name .. '"')
    end

    run_system(state, system_name, ...)
  end

  if _PRINT_RUNNING_SYSTEMS then
    print ''
  end
end

function test_state:update()
  run_systems(self, 'textinput_systems', keys_pressed)
  keys_pressed = {}

  run_systems(self, 'processing_systems')
  run_systems(self, 'event_writers')
  run_systems(self, 'event_systems')
end

function test_state:draw()
  local pos_x, pos_y = love.mouse.getPosition()

  run_systems(self, 'drawing_systems')

  if _USE_PROFILER then
    profi_ticks = profi_ticks + 1
    if profi_ticks == PROFI_LIMIT then
      profi:stop()
      profi:writeReport('profi_report.txt')
    end
  end
end

function love.update()
  test_state:update()
end

function love.draw()
  test_state:draw()
end

function love.textinput(t)
  table.insert(keys_pressed, t)
end

function love.keypressed(key, u)
  if key == 'backspace' or
     key == 'delete' or
     key == 'left' or
     key == 'right'
  then
    table.insert(keys_pressed, key)
  end

   if key == "rctrl" then --set to whatever key you want to use
      debug.debug()
   end
end
