local system_data_lib = require "motor.system_data"

--[[ TODO: handle nil, string
  There is some system_data_lib.get_components calls,
  this function can return 'nil, string',
  this code should handle this case.
--]]

local function process_entity(
  anchored_rect_hierarchy_system_data,
  scene_graph_storage,
  anchored_rect_storage,
  final_anchored_rect_storage,
  idx,
  parent_gl_anchored_rect
)
  local components = system_data_lib.get_components(
    anchored_rect_hierarchy_system_data,
    {scene_graph_storage, anchored_rect_storage, final_anchored_rect_storage},
    idx
  )

  local scene_graph = components[1]
  local anchored_rect = components[2]
  local final_anchored_rect = components[3]

  local local_anchors = anchored_rect.anchors
  local local_offsets = anchored_rect.offsets

  local final_anchors = final_anchored_rect.anchors
  local final_offsets = final_anchored_rect.offsets

  final_anchors.up     = local_anchors.up
  final_anchors.left   = local_anchors.left
  final_anchors.right  = local_anchors.right
  final_anchors.bottom = local_anchors.bottom

  final_offsets.up     = local_offsets.up
  final_offsets.left   = local_offsets.left
  final_offsets.right  = local_offsets.right
  final_offsets.bottom = local_offsets.bottom

  if parent_gl_anchored_rect then
    local parent_anchors, parent_offsets, parent_dimensions, parent_origin =
      parent_gl_anchored_rect.anchors,
      parent_gl_anchored_rect.offsets,
      parent_gl_anchored_rect.canvas_dimensions,
      parent_gl_anchored_rect.origin

    local pos_x =
      (1 - parent_anchors.left)
      * parent_dimensions.width
      + parent_offsets.left

    local pos_y =
      (1 - parent_anchors.up)
      * parent_dimensions.height
      + parent_offsets.up

    local scale_x =
      parent_anchors.right
      * parent_dimensions.width
      - parent_offsets.right
      - pos_x

    local scale_y =
      parent_anchors.bottom
      * parent_dimensions.height
      - parent_offsets.bottom
      - pos_y

    final_anchored_rect.canvas_dimensions.width  = scale_x
    final_anchored_rect.canvas_dimensions.height = scale_y

    final_anchored_rect.origin.x = pos_x + parent_origin.x
    final_anchored_rect.origin.y = pos_y + parent_origin.y
  else
    local w, h = love.graphics.getDimensions()

    final_anchored_rect.canvas_dimensions.width  = w
    final_anchored_rect.canvas_dimensions.height = h

    final_anchored_rect.origin.x = 0
    final_anchored_rect.origin.y = 0
  end

  -- if this entity have children entities
  local children_count = #scene_graph.children_gen_idxs

  if children_count > 0 then
    -- for each child entity
    for j = 1, children_count do
      local child_j = scene_graph.children_gen_idxs[j]
      local child_index = -1

      -- search in what index of components_indexes is this child
      for k = 1, #anchored_rect_hierarchy_system_data.components_indexes do
        local children_components = system_data_lib.get_components(
          anchored_rect_hierarchy_system_data,
          {
            scene_graph_storage,
            anchored_rect_storage,
            final_anchored_rect_storage
          },
          k
        )

        local child_scene_graph = children_components[1]

        if child_scene_graph.entity_gen_idx == child_j then
          child_index = k
          break
        end
      end

      if child_index ~= -1 then
        -- process this child
        process_entity(
          anchored_rect_hierarchy_system_data,
          scene_graph_storage,
          anchored_rect_storage,
          final_anchored_rect_storage,
          child_index,
          final_anchored_rect
        )
      end
    end
  end
end

local function run(
  anchored_rect_hierarchy_system_data,
  scene_graph_storage,
  anchored_rect_storage,
  final_anchored_rect_storage
)
  for i = 1, #anchored_rect_hierarchy_system_data.components_indexes do
    local components = system_data_lib.get_components(
      anchored_rect_hierarchy_system_data,
      {scene_graph_storage, anchored_rect_storage, final_anchored_rect_storage},
      i
    ) or error"error"

    local scene_graph = components[1]

    if not scene_graph.parent_gen_idx then
      process_entity(
        anchored_rect_hierarchy_system_data,
        scene_graph_storage,
        anchored_rect_storage,
        final_anchored_rect_storage,
        i
      )
    end
  end
end

local function new_anchored_rect_hierarchy_system_data(
  scene_graph_mask,
  anchored_rect_mask,
  final_anchored_rect_mask
)
  return system_data_lib.new(
    {scene_graph_mask, anchored_rect_mask},
    {final_anchored_rect_mask}
  )
end

return {
  new = new_anchored_rect_hierarchy_system_data,
  run = run,
  masks_names = {'scene_graph', 'anchored_rect', 'final_anchored_rect'}
}
