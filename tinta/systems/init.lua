local event_system = require 'tinta.systems.event_system'

-- utilities [[
local function union(...)
  local tables, result = {...}, {}
  for i = 1, #tables do
    for j = 1, #tables[i] do
      table.insert(result, tables[i][j])
    end
  end
  return result
end

local function contains_array_of_type(tbl, type_expected)
  if type(tbl) ~= "table" then
    return nil, "is not even a table"
  else
    for i = 1, #tbl do
      if type(tbl[i]) ~= type_expected then
        return nil, "wrong type on [" .. tostring(i) .. '], "' ..
          type_expected .. '" expected, obtained ' .. type(tbl[i])
      end
    end
  end
  return true
end

local function verify_system(system, system_name)
  local function type_msg(vl, type_expected, arr_type_exp)
    local type_obtained = type(vl)

    if type_obtained == type_expected then
      if type_obtained == "table" and arr_type_exp then
        local is_valid, err_msg = contains_array_of_type(vl, arr_type_exp)
        if is_valid then
          return type_expected .. ", -- it's ok here!\n"
        else
          return err_msg
        end
      else
        return type_expected .. ", -- it's ok here!\n"
      end
    else
      return type_obtained .. ', -- expected: ' .. type_expected
    end
  end

  local function system_errmsg()
    return
      'bad types on "' .. system_name .. '": {' ..
        '\n\tnew: ' .. type_msg(system.new, "function") ..
        '\n\trun: ' .. type_msg(system.run, "function") ..
        '\n\tmasks_names: ' .. type_msg(system.masks_names, "table", "string") ..
        '\n}'
  end

  -- is new and run an function?
  -- and is masks_names an array table of strings?
  local is_masks_names_an_array_of_strings =
    contains_array_of_type(system.masks_names, "string")

  assert(
    type(system.new) == "function" and
    type(system.run) == "function" and
    is_masks_names_an_array_of_strings,
    system_errmsg()
  )
end
-- ]]

-- systems to require
local steps = {
  processing_systems = {
    'shape_to_mesh',
    'update_shape_recently_modified',

    'anchored_rect_hierarchy',
    'anchored_rect_to_transform',
    'transform_hierarchy',

    'intersection_detector_system',
    'pointer_system',
    'editable'
  },
  textinput_systems = {
    'editable_text'
  },
  drawing_systems = {
    'mesh_drawer',
    'image_drawer',
    'text_drawer',
    'editable_text_cursor_drawer',
    'anchored_rect_viewer',
  },
  event_writers = {
    'timed_events',
  },
  event_systems = {}
}

local systems_names = union(
  steps.processing_systems,
  steps.textinput_systems,
  steps.event_writers,
  steps.drawing_systems
)

-- requiring systems
local systems = {}

for i = 1, #systems_names do
  local system_name = systems_names[i]
  systems[system_name] = require ("tinta.systems." .. system_name)
  verify_system(systems[system_name], system_name)
end

-- creating event systems
do
  local component_names = require 'tinta.components'.names

  for i = 1, #component_names do
    local component_name = component_names[i]
    local event_system_name = component_name .. '_event_system'

    systems[event_system_name] = event_system.new(component_name)
    table.insert(systems_names, event_system_name)
    table.insert(steps.event_systems, event_system_name)
  end
end

-- verify if all systems table have the expected fields and types
for i = 1, #systems_names do
  local system_name = systems_names[i]
  verify_system(systems[system_name], system_name)
end

return {
  systems = systems,
  names = systems_names,
  names_by_step = steps
}
