local system_data_lib = require 'motor.system_data'
local text_compnt_lib = require 'tinta.components.text'

local function run(
  editable_text_cursor_drawer_system_data,
  final_transform_storage,
  editable_storage,
  text_storage
)
  for final_transform, editable, text in system_data_lib.iterate_components(
    editable_text_cursor_drawer_system_data,
    {final_transform_storage, editable_storage, text_storage},
    true
  ) do
    if editable.in_edit then
      local e1_1, _, _, e1_4,
            e2_1, _, _, e2_4 = final_transform.transform:getMatrix()

      local text_str_before_cursor, _ = text_compnt_lib.split_string(
        text.text_string, editable.cursor_pos
      )

      local font = love.graphics.getFont()

      local letter_width = font:getWidth('i')/2
      local text_str_before_cursor_width =
        font:getWidth(text_str_before_cursor)

      local px, py = e1_4 + text_str_before_cursor_width, e2_4
      local sx, sy = letter_width, text.text:getHeight()

      local r, g, b, a = love.graphics.getColor()
      love.graphics.setColor(1, 1, 1, 0.7)
      love.graphics.rectangle('fill', px, py, sx, sy)
      love.graphics.setColor(r, g, b, a)
    end
  end
end

local function new_editable_text_cursor_drawer_system_data(
  final_transform_mask,
  editable_mask,
  text_mask
)
  return system_data_lib.new({final_transform_mask, editable_mask}, {text_mask})
end

return {
  new = new_editable_text_cursor_drawer_system_data,
  run = run,
  masks_names = {"final_transform", "editable", "text"}
}
