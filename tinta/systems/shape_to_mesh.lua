local system_data_lib = require "motor.system_data"
local shape_to_mesh_lib = require 'tinta.components.shape_to_mesh'

local function run(
  shape_to_mesh_system_data,
  shape_storage,
  shape_to_mesh_storage,
  mesh_storage
)
  for shape, shape_to_mesh, mesh in system_data_lib.iterate_components(
    shape_to_mesh_system_data,
    {shape_storage, shape_to_mesh_storage, mesh_storage},
    true
  ) do
    if shape.recently_modified then
      local vertices = shape_to_mesh_lib.gen_mesh_vertices(shape_to_mesh, shape)
      mesh.mesh:setVertices(vertices)
    end
  end
end

local function new_shape_to_mesh_system_data(
  shape_mask,
  shape_to_mesh_mask,
  mesh_mask
)
  return system_data_lib.new(
    {shape_mask, shape_to_mesh_mask},
    {mesh_mask}
  )
end

return {
  new = new_shape_to_mesh_system_data,
  run = run,
  masks_names = {'shape', 'shape_to_mesh', 'mesh'}
}
