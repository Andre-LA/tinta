local system_data_lib = require "motor.system_data"

local sqrt = math.sqrt

--[[ TODO: handle nil, string
  There is some system_data_lib.get_components calls,
  this function can return 'nil, string',
  this code should handle this case.
--]]

local function process_entity(
  transform_hierarchy_system_data,
  scene_graph_storage,
  transform_storage,
  final_transform_storage,
  idx,
  parent_final_transform
)
  local components = system_data_lib.get_components(
    transform_hierarchy_system_data,
    {scene_graph_storage, transform_storage, final_transform_storage},
    idx
  )

  local scene_graph = components[1]
  local transform = components[2]
  local final_transform = components[3]

  if parent_final_transform then
    local parent_scale_x, parent_scale_y = 1, 1

    if final_transform.using_anchored_rect then
      local  p_e1_1, p_e1_2, _,_,
             p_e2_1, p_e2_2 = parent_final_transform.transform:getMatrix()

      parent_scale_x = sqrt(p_e1_1 * p_e1_1 + p_e1_2 * p_e1_2)
      parent_scale_y = sqrt(p_e2_1 * p_e2_1 + p_e2_2 * p_e2_2)
    end

    final_transform.transform
      :reset()
      :apply(parent_final_transform.transform)
      :scale(1/parent_scale_x, 1/parent_scale_y)
      :apply(transform.transform)
  else
    final_transform.transform
      :reset()
      :apply(transform.transform)
  end

  -- if this entity have children entities
  local children_count = #scene_graph.children_gen_idxs

  if children_count > 0 then
    -- for each child entity
    for j = 1, children_count do
      local child_j = scene_graph.children_gen_idxs[j]
      local child_index = -1

      -- search in what index of components_indexes is this child
      for k = 1, #transform_hierarchy_system_data.components_indexes do
        local children_components = system_data_lib.get_components(
          transform_hierarchy_system_data,
          {
            scene_graph_storage,
            transform_storage,
            final_transform_storage
          },
          k
        )

        local child_scene_graph = children_components[1]

        if child_scene_graph.entity_gen_idx == child_j then
          child_index = k
          break
        end
      end

      if child_index ~= -1  then
        -- process this chil
        process_entity(
          transform_hierarchy_system_data,
          scene_graph_storage,
          transform_storage,
          final_transform_storage,
          child_index,
          final_transform
        )
      end
    end
  end

end

local function run(
  transform_hierarchy_system_data,
  scene_graph_storage,
  transform_storage,
  final_transform_storage
)
  for i = 1, #transform_hierarchy_system_data.components_indexes do
    local components = system_data_lib.get_components(
      transform_hierarchy_system_data,
      {scene_graph_storage, transform_storage, final_transform_storage},
      i
    )

    local scene_graph = components[1]
    if not scene_graph.parent_gen_idx then
      process_entity(
        transform_hierarchy_system_data,
        scene_graph_storage,
        transform_storage,
        final_transform_storage,
        i
      )
    end
  end
end

local function new_transform_hierarchy_system_data(
  scene_graph_mask,
  transform_mask,
  final_transform_mask
)
  return system_data_lib.new(
    {scene_graph_mask, transform_mask},
    {final_transform_mask}
  )
end

return {
  new = new_transform_hierarchy_system_data,
  run = run,
  masks_names = {'scene_graph', 'transform', 'final_transform'}
}
