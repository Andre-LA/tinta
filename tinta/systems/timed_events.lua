local system_data_lib = require "motor.system_data"

local function run(
  timed_events_system_data,
  timed_events_storage,
  events_storage
)
  for timed_events, events in system_data_lib.iterate_components(
    timed_events_system_data, {timed_events_storage, events_storage}, true
  ) do
    local current_time = love.timer.getTime()

    for i = 1, #timed_events.timed_events do
      local timed_event = timed_events.timed_events[i]
      local time_diff = current_time - timed_events.start_time

      if not timed_event.used and time_diff >= timed_event.time then
        timed_event.used = true

        for j = 1, #timed_event.events do
          table.insert(events.events, timed_event.events[j])
        end
      end
    end
  end
end

local function new_timed_events_system_data(timed_events_mask, events_mask)
  return system_data_lib.new(
    {timed_events_mask},
    {events_mask}
  )
end

return {
  new = new_timed_events_system_data,
  run = run,
  masks_names = {'timed_events', 'events'}
}
