local system_data_lib = require "motor.system_data"
local _unpack = require "motor.lua5x-operations".unpack

local function run(
  component_name,
  create_event_system_data,
  event_storage,
  component_storage
)
  for events, component in system_data_lib.iterate_components(
    create_event_system_data, {event_storage, component_storage}, true
  ) do

-- { -- .events
--   { -- [i]
--     'component_name', -- [i][1]
--     { -- [i][2]
--       { -- [i][2][j]
--         "function_name", -- [i][2][j][1]
--         {"table", "of", "arguments"} -- [i][2][j][2]
--       },
--       {"other_function_name", {"table", "of", "arguments"}} -- [i][2][j+1]
--     }
--   }
--   {--[[ other component ]]} -- [i+1]
-- }

    for i = 1, #events.events do
      local event_component_name = events.events[i][1]

      -- is these events we should care about?
      if event_component_name == component_name then
        for j = 1, #events.events[i][2] do
          local function_name = events.events[i][2][j][1]
          local args_values = events.events[i][2][j][2]
          local func = component[function_name]

          if func then
            func(component, _unpack(args_values))
          end
        end

        if not events.fixed_events then
          table.remove(events.events, i)
        end

        break -- break i loop
      end
    end
  end
end

local function new_event_system_data(events_mask, component_mask)
  return system_data_lib.new({}, {events_mask, component_mask})
end

local function new_event_system(component_name)
  return {
    new = new_event_system_data,
    run = function(...) run(component_name, ...) end,
    masks_names = {'events', component_name}
  }
end

return {
  new = new_event_system
}
