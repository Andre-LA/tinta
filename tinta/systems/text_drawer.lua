local system_data_lib = require "motor.system_data"
local text_lib = require 'tinta.components.text'

-- atan2 is deprecated in 5.3, atan should be used instead
local atan2 = math.atan2 or math.atan
local floor = math.floor

local transform_copy = love.math.newTransform()

local function run(
  text_drawer_system_data,
  final_transform_storage,
  text_storage
)
  for final_transform, text in system_data_lib.iterate_components(
    text_drawer_system_data, {final_transform_storage, text_storage}, true
  ) do
    local e1_1, _, _, e1_4,
          e2_1, _, _, e2_4 = final_transform.transform:getMatrix()

    if text.recently_modified then
      text_lib.update_formatting(text, e1_1, e2_1)
      text.recently_modified = false
    end

    transform_copy:reset()
    transform_copy:translate(floor(e1_4), floor(e2_4))
    transform_copy:rotate(atan2(e2_1, e1_1))

    love.graphics.draw(text.text, transform_copy)
  end
end

local function new_text_drawer_system_data(final_transform_mask, text_mask)
  return system_data_lib.new({final_transform_mask, text_mask}, {})
end

return {
  new = new_text_drawer_system_data,
  run = run,
  masks_names = {'final_transform', 'text'}
}
