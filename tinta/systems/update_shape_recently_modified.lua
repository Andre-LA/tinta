local system_data_lib = require "motor.system_data"
local shape_lib = require 'tinta.components.shape'

local function run(update_shape_recently_modified_system_data, shape_storage)
  for shape in system_data_lib.iterate_components(
    update_shape_recently_modified_system_data, {shape_storage}, true
  ) do
    if shape.recently_modified then
      shape_lib.set_bounding_box_from_polygon(shape.bounding_box, shape.vertices)
      shape.recently_modified = false
    end
  end
end

local function new_update_shape_recently_modified(shape_mask)
  return system_data_lib.new({}, {shape_mask})
end

return {
  new = new_update_shape_recently_modified,
  run = run,
  masks_names = {'shape'}
}
