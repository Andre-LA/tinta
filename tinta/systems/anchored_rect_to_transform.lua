local system_data_lib = require "motor.system_data"

local iterate_components = system_data_lib.iterate_components

-- atan2 is deprecated in 5.3, atan should be used instead
local atan2 = math.atan2 or math.atan
local floor = math.floor

local function run(
  anchored_rect_to_transform_system_data,
  final_anchored_rect_storage,
  transform_storage
)
  for final_anchored_rect, transform in iterate_components(
    anchored_rect_to_transform_system_data,
    {final_anchored_rect_storage, transform_storage},
    true
  ) do
    local anchors, offsets, dimensions =
      final_anchored_rect.anchors,
      final_anchored_rect.offsets,
      final_anchored_rect.canvas_dimensions

    local pos_x = (1 - anchors.left) * dimensions.width + offsets.left
    local pos_y = (1 - anchors.up) * dimensions.height + offsets.up

    local scale_x = anchors.right * dimensions.width - offsets.right - pos_x
    local scale_y = anchors.bottom * dimensions.height - offsets.bottom - pos_y

    local e1_1, _, _, _, e2_1 = transform.transform:getMatrix()
    local rotation = atan2(e2_1, e1_1)

    transform.transform:setTransformation(
      floor(pos_x),
      floor(pos_y),
      rotation,
      floor(scale_x),
      floor(scale_y)
    )
  end
end

local function new_anchored_rect_to_transform(
  final_anchored_rect_mask,
  transform_mask
)
  return system_data_lib.new(
    {final_anchored_rect_mask},
    {transform_mask}
  )
end

return {
  new = new_anchored_rect_to_transform,
  run = run,
  masks_names = {'final_anchored_rect', 'transform'}
}
