local system_data_lib = require "motor.system_data"

local function run(
  image_drawer_system_data,
  final_transform_storage,
  image_storage
)
  for final_transform, image in system_data_lib.iterate_components(
    image_drawer_system_data, {final_transform_storage, image_storage}, true
  ) do
    if final_transform.using_anchored_rect then
      local img_width, img_height = image.image:getDimensions()

      final_transform.transform:scale(1/img_width, 1/img_height)
      love.graphics.draw(image.image, final_transform.transform)
      final_transform.transform:scale(img_width, img_height)
    else
      love.graphics.draw(image.image, final_transform.transform)
    end
  end
end

local function new_image_drawer_system_data(final_transform_mask, image_mask)
  return system_data_lib.new({final_transform_mask, image_mask}, {})
end

return {
  new = new_image_drawer_system_data,
  run = run,
  masks_names = {'final_transform', 'image'}
}
