local lua_ops = require 'motor.lua5x-operations'
local system_data_lib = require "motor.system_data"
local intersection_lib = require 'tinta.components.intersection'
local pointer_lib = require 'tinta.components.pointer'

-- if you need to know if is there more buttons pressed,
-- modify this number.
local MOUSE_BUTTONS_TO_VERIFY = 8

local function run(
  pointer_system_data,
  final_transform_storage,
  shape_storage,
  pointer_storage
)
  for final_transform, shape, pointer in system_data_lib.iterate_components(
    pointer_system_data,
    {final_transform_storage, shape_storage, pointer_storage},
    true
  ) do
    local pos_x, pos_y = love.mouse.getPosition()

    local _, _, _, e1_4,
          _, _, _, e2_4 = final_transform.transform:getMatrix()

    pointer.is_pointer_over =
      intersection_lib.check_intersection_between_bounding_box_and_point(
        shape.bounding_box, pos_x - e1_4, pos_y - e2_4
      )

    for i = 1, MOUSE_BUTTONS_TO_VERIFY do
      local button_bit = lua_ops.lshift(1, i-1)

      -- if this button was not pressed before, the result will be 0,
      -- otherwise will be button_bit's value
      local button_was_pressed_bit = lua_ops.band(pointer.buttons, button_bit)
      local button_was_already_pressed = button_was_pressed_bit ~= 0

      if love.mouse.isDown(i) then
        pointer_lib.set_buttons_down_bits(
          pointer, i-1, button_was_pressed_bit == 0
        )
        pointer_lib.set_buttons_up_bits(pointer, i-1, 0)
        pointer_lib.set_buttons_bits(pointer, i-1, 1)
      else
        pointer_lib.set_buttons_down_bits(pointer, i-1, 0)
        pointer_lib.set_buttons_up_bits(
          pointer, i-1, button_was_already_pressed
        )
        pointer_lib.set_buttons_bits(pointer, i-1, 0)
      end
    end
  end
end

local function new_pointer_system(
  final_transform_mask,
  shape_mask,
  pointer_mask
)
  return system_data_lib.new(
    {final_transform_mask, shape_mask},
    {pointer_mask}
  )
end

return {
  new = new_pointer_system,
  run = run,
  masks_names = {'final_transform', 'shape', 'pointer'}
}
