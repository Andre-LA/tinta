local system_data_lib = require 'motor.system_data'
local band = require 'motor.lua5x-operations'.band

local function run(editable_system_data, pointer_storage, editable_storage)
  for pointer, editable in system_data_lib.iterate_components(
    editable_system_data, {pointer_storage, editable_storage}, true
  ) do

    if band(pointer.buttons_down, 1) == 1 then
      editable.in_edit = pointer.is_pointer_over
    end
  end
end

local function new_editable_system_data(pointer_mask, editable_mask)
  return system_data_lib.new({pointer_mask}, {editable_mask})
end

return {
  new = new_editable_system_data,
  run = run,
  masks_names = {"pointer", "editable"}
}
