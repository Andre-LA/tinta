local system_data_lib = require "motor.system_data"

local floor = math.floor

local function draw_rect(px, py, w, h, origin_radius, show_number, number, side)
  px, py, w, h, origin_radius =
    floor(px), floor(py), floor(w), floor(h), floor(origin_radius)

  love.graphics.rectangle('line', px, py, w, h)
  love.graphics.circle('line', px, py, origin_radius)

  if show_number then
    love.graphics.print(side .. ': ' .. number, px+1, py+1)
  end

  return px, py, w, h
end

local function run(
  anchored_rect_system_data,
  final_anchored_rect_storage,
  anchored_rect_viewer_storage
)
  for final_anchored_rect, anchored_rect_viewer in
    system_data_lib.iterate_components( anchored_rect_system_data, {
      final_anchored_rect_storage, anchored_rect_viewer_storage
    }, true
  ) do
    local anchors, offsets, dimensions, origin =
      final_anchored_rect.anchors,
      final_anchored_rect.offsets,
      final_anchored_rect.canvas_dimensions,
      final_anchored_rect.origin

    local pos_x = (1 - anchors.left) * dimensions.width + offsets.left
    local pos_y = (1 - anchors.up) * dimensions.height + offsets.up

    local scale_x = (anchors.right * dimensions.width - offsets.right - pos_x)
    local scale_y = (anchors.bottom * dimensions.height - offsets.bottom - pos_y)

    local show_numbers = anchored_rect_viewer.show_numbers

    -- anchored rect
    love.graphics.setColor(anchored_rect_viewer.anchored_rect_color)
    local dr_ar_x, dr_ar_y =
      draw_rect(origin.x + pos_x, origin.y + pos_y, scale_x, scale_y, 10)

    -- canvas dimensions
    love.graphics.setColor(anchored_rect_viewer.canvas_dimensions_color)
    draw_rect(origin.x, origin.y, dimensions.width, dimensions.height, 7)

    -- anchors up, left, right and bottom respectively
    love.graphics.setColor(anchored_rect_viewer.anchors_color)

    -- anchor up
    local dr_up_x, dr_up_y, dr_up_w, dr_up_h = draw_rect(
      dr_ar_x,
      origin.y,
      scale_x,
      dimensions.height * (1 - anchors.up),
      5,
      show_numbers,
      anchors.up * 100 .. '%',
      'u'
    )

    -- anchor left
    local dr_le_x, dr_le_y, dr_le_w, dr_le_h = draw_rect(
      origin.x,
      dr_ar_y,
      dimensions.width * (1 - anchors.left),
      scale_y,
      5,
      show_numbers,
      anchors.left * 100 .. '%',
      'l'
    )

    -- anchor right
    local dr_ri_x, dr_ri_y, _, dr_ri_h = draw_rect(
      origin.x + dimensions.width * anchors.right,
      dr_ar_y,
      dimensions.width * (1 - anchors.right),
      scale_y,
      5,
      show_numbers,
      anchors.right * 100 .. '%',
      'r'
    )

    -- anchor bottom
    local dr_bo_x, dr_bo_y, dr_bo_w = draw_rect(
      dr_ar_x,
      origin.y + dimensions.height * anchors.bottom,
      scale_x,
      dimensions.height * (1 - anchors.bottom),
      5,
      show_numbers,
      anchors.bottom * 100 .. '%',
      'b'
    )

    -- offsets up, left, right and bottom respectivelyo
    love.graphics.setColor(anchored_rect_viewer.offsets_color)

    draw_rect(
      dr_up_x,
      dr_up_y + dr_up_h,
      dr_up_w,
      offsets.up,
      3,
      show_numbers,
      offsets.up .. 'px',
      'u'
    )

    draw_rect(
      dr_le_x + dr_le_w,
      dr_le_y,
      offsets.left,
      dr_le_h,
      3,
      show_numbers,
      offsets.left .. 'px',
      'l'
    )

    draw_rect(
      dr_ri_x - offsets.right,
      dr_ri_y,
      offsets.right,
      dr_ri_h,
      3,
      show_numbers,
      offsets.right .. 'px',
      'r'
    )

    draw_rect(
      dr_bo_x,
      dr_bo_y - offsets.bottom,
      dr_bo_w,
      offsets.bottom,
      3,
      show_numbers,
      offsets.bottom .. 'px',
      'b'
    )

    love.graphics.setColor(1, 1, 1, 1)
  end
end

local function new_anchored_rect_viewer_system_data(
  final_anchored_rect_mask,
  anchored_rect_viewer_mask
)
  return system_data_lib.new(
    {final_anchored_rect_mask, anchored_rect_viewer_mask},
    {}
  )
end

return {
  new = new_anchored_rect_viewer_system_data,
  run = run,
  masks_names = {'final_anchored_rect', 'anchored_rect_viewer'}
}
