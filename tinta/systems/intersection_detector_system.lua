local system_data_lib = require "motor.system_data"
local intersection_lib = require 'tinta.components.intersection'

local function try_or_error(ok_value, err_msg)
  if not ok_value then
    error("try error: " .. tostring(err_msg))
  end
  return ok_value
end

local function run(
  intersection_detector_system_data,
  final_transform_storage,
  shape_storage,
  intersection_storage
)
  local components_indexes_len =
    #intersection_detector_system_data.components_indexes

  for i = 1, components_indexes_len do
    local detected_intersections = {}

    local components_i = try_or_error(
      system_data_lib.get_components(
        intersection_detector_system_data,
        {final_transform_storage, shape_storage, intersection_storage},
        i
      )
    )

    local intersection_i = components_i[3]
    local shape_i = components_i[2]

    for j = 1, components_indexes_len do
      if i ~= j then
        local components_j = try_or_error(
          system_data_lib.get_components(
            intersection_detector_system_data,
            {final_transform_storage, shape_storage, intersection_storage},
            j
          )
        )

        local shape_j = components_j[2]

        local has_intersection =
          intersection_lib.check_intersection_between_bounding_boxes(
            shape_i.bounding_box,
            shape_j.bounding_box
          )

        if has_intersection then
          table.insert(
            detected_intersections,
            intersection_detector_system_data.components_indexes[j]
          )
        end
      end
    end

    intersection_i.intersections = detected_intersections
  end
end

local function new_intersection_detector_system_data(
  final_transform_mask,
  shape_mask,
  intersection_mask
)
  return system_data_lib.new(
    {final_transform_mask, shape_mask},
    {intersection_mask}
  )
end

return {
  new = new_intersection_detector_system_data,
  run = run,
  masks_names = {'final_transform', 'shape', 'intersection'}
}
