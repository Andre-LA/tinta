local system_data_lib = require "motor.system_data"

local function run(
  mesh_drawer_system_data,
  final_transform_storage,
  mesh_storage
)
  for final_transform, mesh in system_data_lib.iterate_components(
    mesh_drawer_system_data, {final_transform_storage, mesh_storage}, true
  ) do
    love.graphics.draw(mesh.mesh, final_transform.transform)
  end
end

local function new_mesh_drawer_system_data(final_transform_mask, mesh_mask)
  return system_data_lib.new(
    {final_transform_mask, mesh_mask},
    {}
  )
end

return {
  new = new_mesh_drawer_system_data,
  run = run,
  masks_names = {'final_transform', 'mesh'}
}
