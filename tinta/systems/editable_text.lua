local system_data_lib = require 'motor.system_data'
local text_compnt_lib = require 'tinta.components.text'

local _utf8 = utf8 and utf8 or require 'utf8' --> bundled with LÖVE

local function adjust_cursor_pos(editable, text_len)
-- "cursor position clamp"
  if editable.user_cursor_pos > text_len or
     editable.user_cursor_pos < 0
  then
    editable.user_cursor_pos = text_len
  end

  editable.cursor_pos = editable.user_cursor_pos
end

local function run(
  editable_text_system_data,
  editable_storage,
  text_storage,
  keys_pressed
)
  for editable, text in system_data_lib.iterate_components(
    editable_text_system_data, {editable_storage, text_storage}, true
  ) do
    if editable.in_edit and #keys_pressed > 0 then
      local text_str = text.text_string
      local text_len = _utf8.len(text_str)

      for i = 1, #keys_pressed do
        local key_pressed = keys_pressed[i]


        adjust_cursor_pos(editable, text_len)
        local cursor_pos = editable.cursor_pos

        if key_pressed == 'left' then
          -- move cursor position to left
          if editable.user_cursor_pos > 0 then
            editable.user_cursor_pos = editable.user_cursor_pos - 1
          end
        elseif key_pressed == 'right' then
          -- move cursor position to right
          if editable.user_cursor_pos < text_len then
            editable.user_cursor_pos = editable.user_cursor_pos + 1
          end
        elseif key_pressed == 'backspace' or key_pressed == 'delete' then
          -- text deletion
          local text_str_before, text_str_after = text_compnt_lib.split_string(
            text_str, cursor_pos
          )

          if key_pressed == 'delete' then
            if _utf8.len(text_str_after) > 0 then
              text_str_after = string.sub(
                text_str_after,
                _utf8.offset(text_str_after, 2)
              )
            end
          else --backspace
            if _utf8.len(text_str_before) > 0 then
              text_str_before = string.sub(
                text_str_before,
                1,
                _utf8.offset(text_str_before, -1) - 1
              )
            end
          end

          if key_pressed == 'backspace' then
            editable.user_cursor_pos = editable.user_cursor_pos - 1
          end

          local text_result = text_str_before .. text_str_after
          text_compnt_lib.set_text_string(text, text_result)
        else
        -- text insertion
          local text_str_before, text_str_after = text_compnt_lib.split_string(
            text_str, cursor_pos
          )
          -- FIXME: sometimes it doesn't move to right
          editable.user_cursor_pos = editable.user_cursor_pos + 1
          local text_result = text_str_before .. key_pressed .. text_str_after
          text_compnt_lib.set_text_string(text, text_result)
        end
      end

      adjust_cursor_pos(editable, text_len)
    end
  end
end

local function new_editable_text_system_data(editable_mask, text_mask)
  return system_data_lib.new({}, {editable_mask, text_mask})
end

return {
  new = new_editable_text_system_data,
  run = run,
  masks_names = {"editable", "text"}
}
