-- example using text component:
--
-- { -- timed_events argument
--   {
--     time = 3,
--     events = {
--        { -- is the same as events component
--          'text', {
--            {'set_text', {'table', 'of', 'arguments'}},
--            {'set_text', {'table', 'of', 'arguments'}}
--          },
--        }
--     },
--     used = false -- this is for internal use, but is mandatory here
--   }
-- }
--
-- the table above will insert the 'text' event in events after 3 seconds
--

local function start(timed_events)
  timed_events.start_time = love.timer.getTime()

  for i = 1, #timed_events.timed_events do
    timed_events.timed_events[i].used = false
  end
end

local methods = {
  start = start
}

local timed_events_mt = {__index = methods}

local function new(timed_events, start_time)
  local new_timed_events = {
    timed_events = timed_events or {},
    start_time = start_time or math.huge,
  }

  setmetatable(new_timed_events, timed_events_mt)

  return new_timed_events
end

return {
  new = new,
  start = start
}
