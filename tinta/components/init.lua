local components_names = {
  'anchored_rect',
  'anchored_rect_viewer',
  'editable',
  'events',
  'final_anchored_rect',
  'final_transform',
  'image',
  'intersection',
  'mesh',
  'name',
  'scene_graph',
  'shape',
  'shape_to_mesh',
  'text',
  'transform',
  'timed_events',
  'pointer',
}

local components = {}

for i = 1, #components_names do
  local component_name = components_names[i]
  components[component_name] = require ("tinta.components." .. component_name)
end

return {
  components = components,
  names = components_names
}
