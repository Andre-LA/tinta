local lua_ops = require 'motor.lua5x-operations'
local bnot, rshift = lua_ops.bnot, lua_ops.rshift

local simple_type_checking = require 'tinta.simple_type_checking'
local assert_arg_type   = simple_type_checking.assert_arg_type
local assert_table_arg_types = simple_type_checking.assert_table_arg_types
local assert_array_arg_types = simple_type_checking.assert_array_arg_types
local assert_arg_possible_values = simple_type_checking.assert_arg_possible_values

local function new_bounding_box(x, y, w, h)
  return {
    x or 0,
    y or 0,
    w or 0,
    h or 0
  }
end

local function set_bounding_box_from_polygon(bounding_box, poly_coords)
  -- The lowest number possible (I think?)
  local lowest_number = bnot(rshift(bnot(0), 1))  -- ~((~0) >> 1)

  local min_x, min_y, max_x, max_y =
    math.huge, math.huge, lowest_number, lowest_number

  for i=1, #poly_coords, 2 do
    local x, y = poly_coords[i], poly_coords[i+1]

    if x < min_x then
      min_x = x
    elseif x > max_x then
      max_x = x
    end

    if y < min_y then
      min_y = y
    elseif y > max_y then
      max_y = y
    end
  end

  bounding_box[1] = min_x          -- x
  bounding_box[2] = min_y          -- y
  bounding_box[3] = max_x - min_x  -- w
  bounding_box[4] = max_y - min_y  -- h
end

local methods = {
  new_bounding_box = new_bounding_box,
  set_bounding_box_from_polygon = set_bounding_box_from_polygon,
}

local shape_mt = {__index = methods}

local function new (shape_type, shape_data)
  assert_arg_type(shape_type, 'string', 1, 'shape_type', 'shape')
  assert_arg_possible_values(shape_type, {"rectangle", "circle", "polygon"}, 1, "shape_type", "shape")

  local function gen_vertices()
    if shape_type == "rectangle" then
      assert_table_arg_types(shape_data, {width = 'number', height = 'number'}, 2, 'shape_data', 'shape')

      if shape_data.offset ~= nil then
        assert_table_arg_types(shape_data.offset, {x = 'number', y = 'number'}, 2, 'shape_data.offset', 'shape')
      end

      local width  = shape_data.width
      local height = shape_data.height
      local offset = shape_data.offset and shape_data.offset or {x = 0, y = 0}

      return {
--       [          x          ],[          y          ]
        0 + offset.x          , 0 + offset.y         ,
        0 + offset.x + width  , 0 + offset.y         ,
        0 + offset.x + width  , 0 + offset.y + height,
        0 + offset.x          , 0 + offset.y + height
      }
    elseif shape_type == "circle" then
      assert_table_arg_types(shape_data, {radius = 'number', segments = 'number'}, 2, 'shape_data', 'shape')

      if shape_data.offset ~= nil then
        assert_table_arg_types(shape_data.offset, {x = 'number', y = 'number'}, 2, 'shape.offset', 'shape')
      end

      local radius   = shape_data.radius
      local segments = shape_data.segments

      local offset = shape_data.offset ~= nil and shape_data.offset or {x = 0, y = 0}

      local pi, sin, cos = math.pi, math.sin, math.cos

      local vertices = {}

      for i = 1, segments*2, 2 do
        local a = 2 * pi * (i/2 / segments)
        vertices[i]   = cos(a) * radius + offset.x
        vertices[i+1] = sin(a) * radius + offset.y
      end

      return vertices
    elseif shape_type == "polygon" then
      return assert_array_arg_types(shape_data, {'number'}, 2, 'shape_data', 'shape')
    end
  end

  local new_shape = {
    type = shape_type,
    vertices = gen_vertices(),
    bounding_box = new_bounding_box(0,0, 0,0),
    recently_modified = true,
  }

  set_bounding_box_from_polygon(new_shape.bounding_box, new_shape.vertices)

  setmetatable(new_shape, shape_mt)
  return new_shape
end

return {
  new = new,
  new_bounding_box = new_bounding_box,
  set_bounding_box_from_polygon = set_bounding_box_from_polygon,
}
