local simple_type_checking = require 'tinta.simple_type_checking'
local assert_arg_type = simple_type_checking.assert_arg_type

local _utf8 = utf8 and utf8 or require 'utf8' --> bundled with LÖVE

local function split_string(str, position)
  local str_utf8_pos = _utf8.offset(str, position + 1)

  local str_before = string.sub(str, 1, str_utf8_pos - 1)
  local str_after = string.sub(str, str_utf8_pos)
  return str_before, str_after
end

local function calc_wrap_limit(
  tr_matrix_1_1,
  tr_matrix_2_1,
  using_anchored_rect
)
  local wrap_limit = 0

  if not (tr_matrix or using_anchored_rect) then
    wrap_limit = love.graphics.getWidth()
  else
    local abs = math.abs
    wrap_limit = math.sqrt(abs(tr_matrix_1_1 ^ 2) + abs(tr_matrix_2_1 ^ 2))
  end

  return wrap_limit
end

local function update_formatting(
  text,
  tr_matrix_1_1,
  tr_matrix_2_1,
  using_anchored_rect
)
  local wrap_limit = calc_wrap_limit(
    tr_matrix_1_1, tr_matrix_2_1, using_anchored_rect
  )
  text.text:setf(text.text_string, wrap_limit, text.alignment)
end

local function set_text_string(text, text_string)
  text.text_string = text_string
  text.text:set(text_string)
  text.recently_modified = true
end

local methods = {
  split_string = split_string,
  calc_wrap_limit = calc_wrap_limit,
  update_formatting = update_formatting,
  set_text_string = set_text_string,
}

local text_mt = {__index = methods}

local function new (initial_text, alignment)
  local new_text = {
    text_string = assert_arg_type(initial_text, "string", 1, "initial_text", "text"),
    text = love.graphics.newText(love.graphics.getFont(), initial_text),
    recently_modified = false,
    alignment = alignment or 'left'
  }

  setmetatable(new_text, text_mt)

  return new_text
end

return {
  new = new,
  split_string = split_string,
  calc_wrap_limit = calc_wrap_limit,
  update_formatting = update_formatting,
  set_text_string = set_text_string,
}
