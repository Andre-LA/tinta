-- each event entry looks like this:
--
-- { -- events argument in new
--   {
--     'component_name', {
--       {"function_name", {"table", "of", "arguments"}},
--       {"other_function_name", {"table", "of", "arguments"}}
--     }
--   }
-- }
--
-- example:
--
-- new({
--   {'text', {{'set_text', {'A new text'}}}}
--   {'name', {{'set_name', {'a new name'}}}}
-- }, true)
--
-- will run:
-- text['set_text'](text, "a new text")
-- name['set_name'](name, "a new name")
--
-- if component[function_name] doens't exists, it will be ignored
--

local methods = {
}

local events_mt = {__index = methods}

local function new(events, fixed_events)
  local new_events = {
    events = events or {},
    fixed_events = fixed_events or false
  }

  setmetatable(new_events, events_mt)

  return new_events
end

return {
  new = new,
}
