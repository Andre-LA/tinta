-- from: love2d.org/wiki/BoundingBox.lua
local function check_intersection_between_bounding_boxes(bb1, bb2)
  local x1, y1, w1, h1 = bb1[1], bb1[2], bb1[3], bb1[4]
  local x2, y2, w2, h2 = bb2[1], bb2[2], bb2[3], bb2[4]

  return
    x1 < x2+w2 and
    x2 < x1+w1 and
    y1 < y2+h2 and
    y2 < y1+h1
end

local function check_intersection_between_bounding_box_and_point(bb, x, y)
  local bb_x, bb_y, bb_w, bb_h = bb[1], bb[2], bb[3], bb[4]

  return x > bb_x and x < bb_x + bb_w and
         y > bb_y and y < bb_y + bb_h
end

local methods = {
  check_intersection_between_bounding_boxes =
    check_intersection_between_bounding_boxes,
  check_intersection_between_bounding_box_and_point =
    check_intersection_between_bounding_box_and_point
}

local function new ()
  local new_intersection = {
    intersections = {}
  }
  setmetatable(new_intersection, methods)
  return new_intersection
end

return {
  new = new,
  check_intersection_between_bounding_boxes = check_intersection_between_bounding_boxes,
  check_intersection_between_bounding_box_and_point = check_intersection_between_bounding_box_and_point
}
