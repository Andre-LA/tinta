local function new (using_anchored_rect, x, y, angle, sx, sy, ox, oy, kx, ky)
  return {
    transform = love.math.newTransform(x, y, angle, sx, sy, ox, oy, kx, ky),
    using_anchored_rect = using_anchored_rect
  }
end

return {
  new = new
}
