

local methods = {}

local editable_mt = {__index = methods}

local function new()
  local new_editable = {
    in_edit = false,

    -- Note: editable should work for anything, so maybe
    -- "cursor_pos" is too "text especific", this should be rethought
    -- in the future.
    cursor_pos = -1,
    user_cursor_pos = -1
  }

  setmetatable(new_editable, editable_mt)
  return new_editable
end

return {
  new = new,
}
