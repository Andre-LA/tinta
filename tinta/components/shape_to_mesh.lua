local function gen_mesh_vertices(shape_to_mesh, shape)
  local vertices = {}
  local len = 0

  for j=1, #shape.vertices, 2 do
    len = len + 1
    vertices[len] = {
      shape.vertices[j],      --> x
      shape.vertices[j+1],    --> y
      0,                      --> u
      0,                      --> v
      shape_to_mesh.color[1], --> r
      shape_to_mesh.color[2], --> g
      shape_to_mesh.color[3], --> b
      shape_to_mesh.color[4], --> a
    }
  end

  return vertices
end

local methods = {
  gen_mesh_vertices = gen_mesh_vertices
}

local shape_to_mesh_mt = {__index = methods}

local function new (color)
  local new_shape_to_mesh = {
    color = color
  }

  setmetatable(new_shape_to_mesh, shape_to_mesh_mt)
  return new_shape_to_mesh
end

return {
  new = new,
  gen_mesh_vertices = gen_mesh_vertices
}
