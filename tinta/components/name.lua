local simple_type_checking = require 'tinta.simple_type_checking'
local assert_arg_type   = simple_type_checking.assert_arg_type

local function set_name(name, new_name)
  if type(new_name) == 'string' then
    name.name = new_name
  end
end

local methods = {
  set_name
}

local name_mt = {__index = methods}

local function new (name)
  local new_name = {
    name = assert_arg_type(name, "string", 1, "name", "name")
  }

  setmetatable(new_name, name_mt)

  return new_name
end

return {
  new = new,
  set_name
}
