
local function set_image(image, new_image)
  if new_image then
    image.image = love.graphics.newImage(new_image)
  end
end

local methods = {
  set_image = set_image
}

local image_mt = {__index = methods}

local function new(image_data)
  local new_image = {
    image = love.graphics.newImage(image_data)
  }

  setmetatable(new_image, image_mt)

  return new_image
end

return {
  new = new,
  set_image
}
