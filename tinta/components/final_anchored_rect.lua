local assert_table_arg_types =
  require 'tinta.simple_type_checking'.assert_table_arg_types

local function new (anchors, offsets)
  return {
    anchors = assert_table_arg_types(
      anchors, {up = 'number', left = 'number', right = 'number', bottom = 'number'}, 1, 'anchors', 'anchored rect'
    ),
    offsets = assert_table_arg_types(
      offsets, {up = 'number', left = 'number', right = 'number', bottom = 'number'}, 2, 'offsets', 'anchored rect'
    ),
    canvas_dimensions = {
      width = 0,
      height = 0
    },
    origin = {
      x = 0,
      y = 0
    }
  }
end

return {
  new = new
}
