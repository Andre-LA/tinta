local stc  = require 'tinta.simple_type_checking'
local assert_arg_type        = stc.assert_arg_type
local assert_array_arg_types = stc.assert_array_arg_types

local function new (anchored_rect_color, canvas_dimensions_color, anchors_color, offsets_color, show_numbers)
  return {
    anchored_rect_color = assert_array_arg_types(anchored_rect_color, {'number'}, 1, 'anchored_rect_color', 'anchored_rect_viewer'),
    canvas_dimensions_color = assert_array_arg_types(canvas_dimensions_color, {'number'}, 2, 'canvas_dimensions_color', 'anchored_rect_viewer'),
    anchors_color = assert_array_arg_types(anchors_color, {'number'}, 3, 'anchors_color', 'anchored_rect_viewer'),
    offsets_color = assert_array_arg_types(offsets_color, {'number'}, 4, 'offsets_color', 'anchored_rect_viewer'),
    show_numbers  = assert_arg_type(show_numbers, "boolean", 5, "show_numbers", "anchored_rect_viewer")
  }
end

return {
  new = new
}
