local function new (arg_a, arg_b, arg_c, arg_d) -- same as in Löve
  return {
    mesh = love.graphics.newMesh(arg_a, arg_b, arg_c, arg_d)
  }
end

return {
  new = new
}
