local assert_table_arg_types =
  require 'tinta.simple_type_checking'.assert_table_arg_types

local function new(anchors, offsets)
  local _anchors = assert_table_arg_types(
    anchors, {up = 'number', left = 'number', right = 'number', bottom = 'number'}, 1, 'anchors', 'anchored rect'
  )

  local _offsets = assert_table_arg_types(
    offsets, {up = 'number', left = 'number', right = 'number', bottom = 'number'}, 2, 'offsets', 'anchored rect'
  )

  return {
    anchors = _anchors,
    offsets = _offsets,
  }
end


return {
  new = new,
}
