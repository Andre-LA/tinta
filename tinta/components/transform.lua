local function new (x, y, angle, sx, sy, ox, oy, kx, ky)
  return {
    transform = love.math.newTransform(x, y, angle, sx, sy, ox, oy, kx, ky)
  }
end

return {
  new = new
}
