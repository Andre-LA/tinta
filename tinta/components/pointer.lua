local lua_ops = require 'motor.lua5x-operations'

local lshift, bnot, band, bor =
  lua_ops.lshift, lua_ops.bnot, lua_ops.band, lua_ops.bor

local function assert_value_type_and_convert_to_integer(value)
  if value == true or value == 1 then
    return 1
  elseif value == false or value == 0 then
    return 0
  else
    local vl_str = tostring(value)
    error('bad argument #3, "value" must be a boolean or 1 or 0, got '.. vl_str)
  end
end

local function set_bit(number, index, value)
  local index_bit = lshift(1, index)
  local value_bit = lshift(value, index)

  return bor(
    band(number, bnot(index_bit)), -- clear this bit
    value_bit -- set this bit
  )
end

-- note: value must be either a boolean or 1 or 0
local function set_buttons_bits(pointer, index, value)
  local value_as_integer = assert_value_type_and_convert_to_integer(value)
  pointer.buttons = set_bit(pointer.buttons, index, value_as_integer)
end

-- note: value must be either a boolean or 1 or 0
local function set_buttons_down_bits(pointer, index, value)
  local value_as_integer = assert_value_type_and_convert_to_integer(value)
  pointer.buttons_down = set_bit(pointer.buttons_down, index, value_as_integer)
end

-- note: value must be either a boolean or 1 or 0
local function set_buttons_up_bits(pointer, index, value)
  local value_as_integer = assert_value_type_and_convert_to_integer(value)
  pointer.buttons_up = set_bit(pointer.buttons_up, index, value_as_integer)
end

local methods = {
  set_buttons_bits = set_buttons_bits,
  set_buttons_down_bits = set_buttons_down_bits,
  set_buttons_up_bits = set_buttons_up_bits
}

local pointer_mt = {__index = methods}

local function new_pointer()
  local new_pointer = {
    is_pointer_over = false,
    buttons = 0,
    buttons_up = 0,
    buttons_down = 0,
  }

  setmetatable(new_pointer, pointer_mt)
  return new_pointer
end

return {
  new = new_pointer,
  set_buttons_bits = set_buttons_bits,
  set_buttons_down_bits = set_buttons_down_bits,
  set_buttons_up_bits = set_buttons_up_bits
}
