local function new (entity_gen_idx, parent_gen_idx, children_gen_idxs)
  return {
    entity_gen_idx = entity_gen_idx,
    parent_gen_idx = parent_gen_idx or false,
    children_gen_idxs = children_gen_idxs or {}
  }
end

return {
  new = new
}
