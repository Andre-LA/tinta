local function t_wrong_type_x_expected_got_y(thing, component, type_expected, value_type, arg_number)
  return
    (arg_number and "Argument #" .. arg_number .. ' "' or '"')
    .. thing .. '" with wrong type to component "' .. component .. '" ("'
    .. type_expected .. '" expected, got "' .. value_type .. '").'
end

local function check_table_types(tbl, expected, tbl_name, component, arg_number)
  local ok, err = true;

  if type(tbl) ~= 'table' then
    ok = false
    err = t_wrong_type_x_expected_got_y(tbl_name, component, 'table', type(tbl), arg_number)
  else
    for key, type_expected in pairs(expected) do
      if tbl[key] == nil then
        ok = false
        err = (arg_number and 'Argument #' .. arg_number .. ' "' or '"')
          .. tbl_name .. '" table does not contain the "' .. key .. '" key.'
      elseif type(tbl[key]) ~= type_expected then
        if type(type_expected) ~= 'table' then
          ok = false
          err = t_wrong_type_x_expected_got_y(
            tbl_name .. '.' .. key, component, type_expected, type(tbl[key]), arg_number
          )
        else
          ok, err = check_table_types(tbl[key], type_expected, tbl_name .. '["' .. key .. '"]', component)
        end
      end
    end
  end

  return (ok and tbl or ok), err
end

local function assert_arg_type(arg_value, type_expected, arg_number, arg_name, component)
  local value_type = type(arg_value)

  assert(
    value_type == type_expected,
    t_wrong_type_x_expected_got_y(
      arg_name,
      component,
      type_expected,
      value_type,
      arg_number
    )
  )

  return arg_value
end

local function assert_table_arg_types(arg_value, types_expected, arg_number, arg_name, component)
  assert(check_table_types(
    arg_value, types_expected, arg_name, component, arg_number
  ))

  return arg_value
end

local function assert_array_arg_types(arg_value, types_expected, arg_number, arg_name, component)
  local types_i, types_limit = 1, #types_expected

  for i = 1, #arg_value do
    local arg_value_i_type, expected_type_i = type(arg_value[i]), types_expected[types_i]

    assert(
      arg_value_i_type == expected_type_i,
      t_wrong_type_x_expected_got_y(
        arg_name .. '[' .. i .. ']',
        component,
        expected_type_i,
        arg_value_i_type,
        arg_number
      )
    )

    types_i = types_i + 1
    if types_i > types_limit then
      types_i = 1
    end
  end

  return arg_value
end

local function assert_arg_possible_values(arg_value, possible_values, arg_number, arg_name, component)

  local ok = false
  for i = 1, #possible_values do
    ok = ok or arg_value == possible_values[i]
  end

  assert(
    ok,
    'Argument #' .. arg_number .. ' "' .. arg_name .. '" is not equal to any of the accepted values ('
    .. table.concat(possible_values, '|') .. ') to "' .. component
    .. '", instead it is equal to "' .. arg_value .. '"'
  )

  return arg_value
end

return {
  assert_arg_type = assert_arg_type,
  check_table_types = check_table_types,
  assert_table_arg_types = assert_table_arg_types,
  assert_array_arg_types = assert_array_arg_types,
  assert_arg_possible_values = assert_arg_possible_values
}
