# Tinta

Artista components implemented  in [Motor][motor-repository] for [LÖVE][love2d-website]

(WIP) Components for a 1st draft:

- [x] Anchored Rect
- [x] Anchored Rect Viewer
- [x] Editable (but is text focused instead of a generic one :[ )
- [x] Timed Events
- [x] Pointer
- [x] Events
- [x] Final Anchored Rect
- [x] Final Transform
- [x] Image
- [x] Intersection
- [x] Mesh
- [x] Name
- [x] Scene Graph
- [x] Shape
- [x] Shape to Mesh
- [x] Text
- [x] Transform

(WIP) Systems for a 1st draft:

- [x] Anchored Rect Hierarchy
- [x] Anchored Rect to Transform
- [x] Anchored Rect Viewer
- [x] Timed Events
- [x] Pointer
- [x] Editable (but is text focused implemetation instead of a generic one :[ )
- [x] Editable Text
- [x] Editable Text Cursor Drawer
- [x] Event Systems
- [x] Image Drawer
- [x] Intersection Detector
- [x] Mesh Drawer
- [x] Shape to Mesh
- [x] Text Drawer
- [x] Transform Hierarchy
- [x] Update Shape Recently Modified

[love2d-website]: http://love2d.org/
[motor-repository]: https://github.com/Andre-LA/Motor
